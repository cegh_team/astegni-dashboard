import 'package:flutter/material.dart';
import '../responsive.dart';
import 'header.dart';
import 'my_users.dart';

class DashboardPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        color: Color(0xFFf7f7f7), //Theme.of(context).backgroundColor,
        child: SingleChildScrollView(
          padding: EdgeInsets.all(16),
          child: Column(
            children: [
              Header(),
              SizedBox(height: 16),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 5,
                    child: Column(
                      children: [
                        MyUsers(),
                        if (Responsive.isMobile(context)) SizedBox(height: 16),
                      ],
                    ),
                  ),
                  if (!Responsive.isMobile(context)) SizedBox(width: 16),
                ],
              ),
              // SizedBox(
              //   height: 10,
              // ),
              // TutorsPage(),
              if (!Responsive.isMobile(context))
                SizedBox(
                  height: 300,
                ),
            ],
          ),
        ),
      ),
    );
  }
}
