import 'package:astegni_dashboard/data/models/user.dart';
import 'package:astegni_dashboard/data/repositaries/tutor_repository.dart';
import 'package:astegni_dashboard/logic/blocs/tutor/tutor_bloc.dart';
import 'package:astegni_dashboard/logic/blocs/tutor/tutor_event.dart';
import 'package:astegni_dashboard/logic/blocs/tutor/tutor_state.dart';
import 'package:astegni_dashboard/presentation/dashboard/constants.dart';
import 'package:astegni_dashboard/presentation/dashboard/side_menu.dart';
import 'package:astegni_dashboard/presentation/router/route_constants.dart';
import 'package:astegni_dashboard/utilities/base_url.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../responsive.dart';
import 'header.dart';

class TutorsPage extends StatefulWidget {
  const TutorsPage({Key? key}) : super(key: key);

  @override
  _TutorsPageState createState() => _TutorsPageState();
}

class _TutorsPageState extends State<TutorsPage> {
  String? _selected = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideMenu(),
      body: SafeArea(
        child: Row(
          children: [
            if (Responsive.isDesktop(context))
              Expanded(
                child: SideMenu(),
              ),
            Expanded(
              flex: 5,
              child: RepositoryProvider(
                create: (context) => TutorRepository(),
                child: BlocProvider(
                  create: (context) => TutorBloc(
                      tutorRepository: context.read<TutorRepository>())
                    ..add(TutorLoad()),
                  child: BlocBuilder<TutorBloc, TutorState>(
                    builder: (context, state) {
                      if (state is TutorOperationFailure) {
                        return Center(child: Text('Could not load tutors'));
                      }

                      if (state is TutorsLoadSuccess) {
                        final tutors = state.tutors;

                        return _tutorsPageBody(context, tutors);
                      }
                      return Center(
                        child: Container(
                          height: 100,
                          width: 100,
                          child: CircularProgressIndicator(),
                        ),
                      );
                    },
                  ),
                ),
              ),
              /////////
            ),
          ],
        ),
      ),
    );
  }

  Widget _tutorsPageBody(context, tutors) {
    return SafeArea(
      child: ListView(
        children: [
          Container(
            // color: Theme.of(context).backgroundColor,
            padding: EdgeInsets.all(defaultPadding),
            decoration: BoxDecoration(
              color: Color(0xFFf7f7f7),
              borderRadius: const BorderRadius.all(Radius.circular(10)),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Header(),
                SizedBox(height: 16),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Tutors",
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                  ],
                ),
                SizedBox(
                  width: double.infinity,
                  child: DataTable2(
                    columnSpacing: defaultPadding,
                    minWidth: 600,
                    columns: [
                      DataColumn(
                        label: Text(
                          "Name",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      DataColumn(
                        label: Text(
                          "Biography",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      DataColumn(
                        label: Text(
                          "Status",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      DataColumn(
                        label: Text(
                          "Actions",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                    rows: List.generate(
                      tutors.length,
                      (index) => tutorDataRow(tutors[index], context),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  DataRow tutorDataRow(User tutor, context) {
    return DataRow(
      cells: [
        DataCell(
          Row(
            children: [
              Container(
                  height: 20,
                  width: 20,
                  child: Image.network(
                      "$PROFILE_IMAGE_BASE_URL${tutor.profilePicture}")),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
                child: Text("${tutor.firstName} ${tutor.lastName}"),
              ),
            ],
          ),
        ),
        DataCell(Text(tutor.bio)),
        //show using on/off button
        DataCell(
          BlocBuilder<TutorBloc, TutorState>(
            builder: (context, state) {
              return Switch(
                value: (tutor.status == "false") ? false : true,
                onChanged: (value) {
                  context
                      .read<TutorBloc>()
                      .add(TutorStatusUpdate(tutor.userId, value.toString()));
                },
                activeTrackColor: Colors.yellow,
                activeColor: Color(0xFF3EB489),
              );
            },
          ),
        ),
        DataCell(
          Row(
            children: [
              ActionButton(
                icon: Icon(
                  Icons.more_rounded,
                  color: Theme.of(context).primaryColor,
                ),
                press: () {
                  _openDialog(
                      "Tutor Details",
                      _tutorDetails(
                          tutor.userId,
                          tutor.status,
                          "$PROFILE_IMAGE_BASE_URL/${tutor.profilePicture}",
                          tutor.firstName,
                          tutor.lastName,
                          tutor.email,
                          tutor.gender,
                          tutor.availability,
                          tutor.pricing,
                          tutor.address,
                          tutor.bio,
                          tutor.subjects,
                          tutor.phone));
                },
              ),
              // ActionButton(
              //   icon: Icon(
              //     Icons.edit,
              //     color: Theme.of(context).primaryColor,
              //   ),
              //   press: () {
              //     Navigator.of(context).pushNamed(AstegniRoutes.homeRoute);
              //   },
              // ),
              BlocBuilder<TutorBloc, TutorState>(
                builder: (context, state) {
                  return ActionButton(
                    icon: Icon(
                      Icons.delete,
                      color: Colors.red,
                    ),
                    press: () {
                      context.read<TutorBloc>().add(TutorDelete(tutor));
                    },
                  );
                },
              ),
            ],
          ),
        ),
      ],
    );
  }

  void _openDialog(String actionName, Widget widget) async {
    _selected = await Navigator.of(context).push(new MaterialPageRoute<String>(
        builder: (BuildContext context) {
          return new Scaffold(
            appBar: new AppBar(
              title: Text('$actionName'),
              actions: [],
            ),
            body: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                padding: EdgeInsets.all(20),
                color: Colors.white,
                child: widget),
          );
        },
        fullscreenDialog: true));
    if (_selected != null)
      setState(() {
        _selected = _selected;
      });
  }

  Widget _tutorDetails(userId, status, picture, firstName, lastName, email,
      gender, availability, pricing, address, bio, subjects, phone) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(80.0),
        child: ListView(
          children: [
            //image and other infos
            Row(
              children: [
                Container(
                  height: 300,
                  width: 400,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10.0),
                    child: Image.network(picture),
                  ),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text("Name: $firstName $lastName",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20)),
                      ),
                      SizedBox(height: 20),
                      Text("Email: $email",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20)),
                      SizedBox(height: 20),
                      Text("Phone Number: $phone",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20)),
                      SizedBox(height: 20),
                      Text("Location: $address",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20)),
                      SizedBox(height: 20),
                      Text("Gender: $gender",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20)),
                      SizedBox(height: 20),
                      Text("Pricing: $pricing",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20)),
                      SizedBox(height: 20),
                      Text(
                          "Availability: ${availability == "true" ? "Available" : "Unavailbale"}",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20)),
                      SizedBox(height: 20),
                      Text(
                          "Status: ${status == "false" ? "Inactive" : "active"}",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20)),
                    ]),
                  ),
                ),
              ],
            ),
            //bio
            Padding(
              padding: const EdgeInsets.only(bottom: 20, top: 20),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 10),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "BIOGRAPHY",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 25,
                            color: Theme.of(context).primaryColor),
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: SizedBox(
                      width: 700,
                      child: Text(
                        bio,
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                  )
                ],
              ),
            ),
            //subjects
            Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "SUBJECTS",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 25,
                          color: Theme.of(context).primaryColor),
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  child: SizedBox(
                    width: 200,
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: subjects.length,
                      itemBuilder: (_, id) => Row(
                        children: [
                          Card(
                            color: Theme.of(context).primaryColor,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                '${subjects[id].name}',
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.white60,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class ActionButton extends StatelessWidget {
  const ActionButton({
    Key? key,
    required this.icon,
    required this.press,
  }) : super(key: key);

  final Widget icon;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      height: 30,
      onPressed: press,
      shape: CircleBorder(),
      child: icon,
    );
  }
}
