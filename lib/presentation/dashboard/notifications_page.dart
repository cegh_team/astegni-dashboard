import 'package:astegni_dashboard/data/models/notification.dart';
import 'package:astegni_dashboard/data/repositaries/notification_repository.dart';
import 'package:astegni_dashboard/logic/blocs/notification/bloc.dart';
import 'package:astegni_dashboard/presentation/dashboard/constants.dart';
import 'package:astegni_dashboard/presentation/dashboard/side_menu.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../responsive.dart';
import 'header.dart';

class NotificationsPage extends StatelessWidget {
  const NotificationsPage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideMenu(),
      body: SafeArea(
        child: Row(
          children: [
            if (Responsive.isDesktop(context))
              Expanded(
                child: SideMenu(),
              ),
            Expanded(
              flex: 5,
              child: RepositoryProvider(
                create: (context) => NotificationRepository(),
                child: BlocProvider(
                  create: (context) => NotificationBloc(
                      notificationRepository:
                          context.read<NotificationRepository>())
                    ..add(NotificationLoad()),
                  child: BlocBuilder<NotificationBloc, NotificationState>(
                    builder: (context, state) {
                      if (state is NotificationOperationFailure) {
                        return Center(
                            child: Text('Could not load Notifications'));
                      }

                      if (state is NotificationsLoadSuccess) {
                        final notifications = state.notifications;

                        return _notificationsPageBody(context, notifications);
                      }
                      return Center(
                        child: Container(
                          height: 100,
                          width: 100,
                          child: CircularProgressIndicator(),
                        ),
                      );
                    },
                  ),
                ),
              ),
              /////////
            ),
          ],
        ),
      ),
    );
  }
}

Widget _notificationsPageBody(context, notifications) {
  return SafeArea(
    child: Container(
      // color: Theme.of(context).backgroundColor,
      padding: EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        color: Color(0xFFf7f7f7), //Theme.of(context).backgroundColor,
        //color: Theme.of(context).backgroundColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Header(),
          SizedBox(height: 16),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Notifications",
                style: Theme.of(context).textTheme.subtitle1,
              ),
            ],
          ),
          SizedBox(
            width: double.infinity,
            child: DataTable2(
              columnSpacing: defaultPadding,
              minWidth: 600,
              columns: [
                DataColumn(
                  label: Text(
                    "Title",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                DataColumn(
                  label: Text(
                    "Content",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                DataColumn(
                  label: Text(
                    "Sender ID",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                DataColumn(
                  label: Text(
                    "Actions",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ],
              rows: List.generate(
                notifications.length,
                (index) => notificationDataRow(notifications[index], context),
              ),
            ),
          ),
        ],
      ),
    ),
  );
}

DataRow notificationDataRow(NotificationModel notification, context) {
  return DataRow(
    cells: [
      DataCell(Text(notification.title)),
      DataCell(Text(notification.content)),
      //show using on/off button
      DataCell(Text(notification.senderId)),
      DataCell(
        Row(
          children: [
            Icon(
              Icons.details,
              color: Theme.of(context).primaryColor,
            ),
            SizedBox(
              width: 10,
            ),
            Icon(
              Icons.edit,
              color: Theme.of(context).primaryColor,
            ),
            SizedBox(
              width: 10,
            ),
            Icon(
              Icons.delete,
              color: Colors.red,
            ),
          ],
        ),
      ),
    ],
  );
}
