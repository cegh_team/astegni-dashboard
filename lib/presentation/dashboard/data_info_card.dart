import 'package:astegni_dashboard/data/models/MyUsers.dart';
import 'package:flutter/material.dart';

import 'constants.dart';

class DataInfoCard extends StatelessWidget {
  const DataInfoCard({
    Key? key,
    required this.info,
  }) : super(key: key);

  final DataInfo info;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        color: Color(0xFFDADADA),
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Icon(Icons.account_circle, color: Theme.of(context).primaryColor),
              SizedBox(
                width: 5,
              ),
              Text(
                info.title!,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(fontWeight: FontWeight.bold),

                // style: TextStyle(color: Theme.of(context).primaryColor),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "${info.numOfUsers}",
                style: TextStyle(fontWeight: FontWeight.bold),
                // style: Theme.of(context)
                //     .textTheme
                //     .caption!
                //     .copyWith(color: Theme.of(context).primaryColor),
              )
            ],
          )
        ],
      ),
    );
  }
}
