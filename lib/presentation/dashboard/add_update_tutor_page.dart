import 'package:astegni_dashboard/data/repositaries/tutor_repository.dart';
import 'package:astegni_dashboard/logic/blocs/tutor/tutor_bloc.dart';
import 'package:astegni_dashboard/logic/blocs/tutor/tutor_event.dart';
import 'package:astegni_dashboard/logic/blocs/tutor/tutor_state.dart';
import 'package:astegni_dashboard/presentation/responsive.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AddUpdateTutorPage extends StatelessWidget {
  const AddUpdateTutorPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: SafeArea(
        child: RepositoryProvider(
          create: (context) => TutorRepository(),
          child: BlocProvider(
            create: (context) =>
                TutorBloc(tutorRepository: context.read<TutorRepository>()),
            child: _addTutorForm(context),
          ),
        ),
      ),
    );
  }

  Widget _addTutorForm(context) {
    return Container(
      child: ListView(
        children: [
          Center(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("ADD TUTOR",
                  style: TextStyle(
                      fontSize: 30, color: Theme.of(context).primaryColor)),
            ),
          ),
          Divider(
            height: 5,
            color: Colors.black54,
            thickness: 1,
            endIndent: 15,
            indent: 15,
          ),
          Responsive.isDesktop(context)
              ? Row(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.5,
                      child: Column(
                        children: [
                          _firstNameField(),
                          _sizedBox(),
                          _lastNameField(),
                          _sizedBox(),
                          _emailField(),
                          _sizedBox(),
                          _roleSelectionField(),
                          _availabilityField(),
                        ],
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.5,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        // mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          ListTile(title: Text("Select Gender")),
                          _genderField(),
                          _sizedBox(),
                          _aboutMeTextArea(),
                          _sizedBox(),
                          _addressField(),
                          _sizedBox(),
                          _pricingField(),
                          _sizedBox(),
                          _passwordField(),
                          _sizedBox(),
                          _sizedBox(),
                          _saveChangesButton(context),
                        ],
                      ),
                    ),
                  ],
                )
              : Column(
                  children: [
                    _firstNameField(),
                    _sizedBox(),
                    _lastNameField(),
                    _sizedBox(),
                    _emailField(),
                    _sizedBox(),
                    _roleSelectionField(),
                    _sizedBox(),
                    _availabilityField(),
                    _sizedBox(),
                    ListTile(title: Text("Select Gender")),
                    _genderField(),
                    _sizedBox(),
                    _aboutMeTextArea(),
                    _sizedBox(),
                    _addressField(),
                    _sizedBox(),
                    _pricingField(),
                    _sizedBox(),
                    _passwordField(),
                    _sizedBox(),
                    _sizedBox(),
                    _saveChangesButton(context),
                  ],
                ),
          _sizedBox(),
          _sizedBox(),
          _sizedBox(),
          _sizedBox(),
          _sizedBox(),
          _sizedBox(),
        ],
      ),
    );
  }

  Widget _sizedBox() {
    return SizedBox(height: 10);
  }

  Widget _firstNameField() {
    return BlocBuilder<TutorBloc, TutorState>(
      builder: (context, state) {
        return ListTile(
          title: TextFormField(
            onChanged: (String firstName) {
              context.read<TutorBloc>().add(TutorFirstNameChanged(firstName));
            },
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(6.0),
              labelText: state.firstname == "" ? "First Name" : state.firstname,
              hintText: "First Name",
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _lastNameField() {
    return BlocBuilder<TutorBloc, TutorState>(
      builder: (context, state) {
        return ListTile(
          title: TextFormField(
            onChanged: (String lastName) {
              context.read<TutorBloc>().add(TutorLastNameChanged(lastName));
            },
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(6.0),
              labelText: state.lastname == "" ? "last Name" : state.lastname,
              hintText: "Last Name",
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _emailField() {
    return BlocBuilder<TutorBloc, TutorState>(
      builder: (context, state) {
        return ListTile(
          title: TextFormField(
            // initialValue: state.email,
            onChanged: (String email) {
              context.read<TutorBloc>().add(TutorEmailChanged(email));
            },
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(6.0),
              labelText: state.email == "" ? "email" : state.email,
              hintText: "Your Email",
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _passwordField() {
    return BlocBuilder<TutorBloc, TutorState>(
      builder: (context, state) {
        return ListTile(
          title: TextFormField(
            initialValue: state.password,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
              ),
              prefixIcon: Icon(
                Icons.lock,
                color: Theme.of(context).primaryColor,
              ),
              // icon: const Icon(Icons.lock),
              helperText: ''' 6 char long, enclude  number''',
              helperMaxLines: 1,
              labelText: 'Password',
            ),
            obscureText: true,
            onChanged: (value) {
              context.read<TutorBloc>().add(TutorPasswordChanged(value));
            },
            textInputAction: TextInputAction.next,
          ),
        );
      },
    );
  }

  Widget _roleSelectionField() {
    return BlocBuilder<TutorBloc, TutorState>(
      buildWhen: (previous, current) => previous.role != current.role,
      builder: (context, state) {
        return ListTile(
          title: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Add user as:",
                    style: TextStyle(fontSize: 20, color: Colors.grey),
                  ),
                ],
              ),
              ListTile(
                title: const Text('Student'),
                leading: Radio<String>(
                  value: 'student',
                  groupValue: state.role,
                  onChanged: (value) {
                    context.read<TutorBloc>().add(TutorRoleChanged(value!));
                  },
                ),
              ),
              ListTile(
                title: const Text('Tutor'),
                leading: Radio<String>(
                  value: 'tutor',
                  groupValue: state.role,
                  onChanged: (value) {
                    context.read<TutorBloc>().add(TutorRoleChanged(value!));
                  },
                ),
              ),
              ListTile(
                title: const Text('Admin'),
                leading: Radio<String>(
                  value: 'admin',
                  groupValue: state.role,
                  onChanged: (value) {
                    context.read<TutorBloc>().add(TutorRoleChanged(value!));
                  },
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  Widget _addressField() {
    return BlocBuilder<TutorBloc, TutorState>(
      builder: (context, state) {
        return ListTile(
          title: TextFormField(
            // initialValue: state.address,
            onChanged: (String address) {
              context.read<TutorBloc>().add(TutorAddressChanged(address));
            },
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(6.0),
              labelText: state.address == "" ? "Address" : state.address,
              hintText: "Your address(eg. Addis Ababa)",
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _genderField() {
    return BlocBuilder<TutorBloc, TutorState>(
      builder: (context, state) {
        return state.gender == ''
            ? ListTile(
                title: DropdownButton(
                    value: "male",
                    icon: Icon(Icons.keyboard_arrow_down),
                    elevation: 16,
                    style: const TextStyle(color: Colors.grey),
                    underline: Container(
                      height: 3,
                      color: Colors.grey,
                    ),
                    items: ['male', 'female'].map((String items) {
                      return DropdownMenuItem(value: items, child: Text(items));
                    }).toList(),
                    onChanged: (String? newValue) {
                      context
                          .read<TutorBloc>()
                          .add(TutorGenderChanged(newValue!));
                      print("changed");
                      //set state of dropdown value here
                    }),
              )
            : ListTile(
                title: Text("Gender: ${state.gender}"),
              );
      },
    );
  }

  Widget _pricingField() {
    return BlocBuilder<TutorBloc, TutorState>(
      builder: (context, state) {
        return ListTile(
          title: TextFormField(
            keyboardType: TextInputType.number,
            // initialValue: state.pricing,
            onChanged: (String pricing) {
              context.read<TutorBloc>().add(TutorPricingChanged(pricing));
            },
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(6.0),
              hintText: "Pricing BR/Hr",
              labelText: state.pricing == "" ? 'Pricing' : state.pricing,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _aboutMeTextArea() {
    return BlocBuilder<TutorBloc, TutorState>(
      builder: (context, state) {
        return ListTile(
          title: TextFormField(
            onChanged: (String aboutme) {
              context.read<TutorBloc>().add(TutorUserBioChanged(aboutme));
            },
            maxLines: 8,
            initialValue: state.userBio,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(6.0),
              labelText: "biography",
              // hintText:
              //     state.bio == "" ? "Write your bio here" : state.bio,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _availabilityField() {
    return BlocBuilder<TutorBloc, TutorState>(
      builder: (context, state) {
        return Column(
          children: [
            ListTile(
              title: Text("Are you available?"),
            ),
            Row(
              children: [
                Expanded(
                  child: ListTile(
                    title: const Text('YES'),
                    leading: Radio<String>(
                      value: 'yes',

                      groupValue: state
                          .availability, // == "" ? state.availability : state.availability,
                      onChanged: (value) {
                        context.read<TutorBloc>().add(
                            TutorAvailabilityChanged(availability: value!));
                      },
                    ),
                  ),
                ),
                Expanded(
                  child: ListTile(
                    title: const Text('NO'),
                    leading: Radio<String>(
                      value: 'no',
                      groupValue: state.availability,
                      onChanged: (value) {
                        context.read<TutorBloc>().add(
                            TutorAvailabilityChanged(availability: value!));
                      },
                    ),
                  ),
                ),
              ],
            ),
          ],
        );
      },
    );
  }

  Widget _saveChangesButton(context) {
    return BlocBuilder<TutorBloc, TutorState>(
      builder: (context, state) {
        return Container(
          // width: MediaQuery.of(context).size.width * 0.5,
          child: ListTile(
            title: MaterialButton(
              height: 55,
              onPressed: () {
                // Navigator.pop(context);
                context.read<TutorBloc>().add(TutorSubmitted());
              },
              shape: StadiumBorder(),
              color: Theme.of(context).primaryColor,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Save",
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
