import 'package:astegni_dashboard/presentation/router/route_constants.dart';
import 'package:astegni_dashboard/presentation/values/images.dart';
import 'package:flutter/material.dart';

class SideMenu extends StatelessWidget {
  const SideMenu({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: Color(0xFFDADADA), //Theme.of(context).primaryColor,
        child: ListView(
          children: [
            DrawerHeader(
              child: Row(
                children: [
                  Container(
                    height: 1000,
                    //color: Colors.white60,
                    child: Image.asset(Images.logo),
                  ),
                ],
              ),
            ),
            DrawerListTile(
              title: "Dashboard",
              icon: Icon(
                Icons.dashboard,
                color: Color(0xFF018790),
              ),
              press: () {
                Navigator.of(context).pushNamed(AstegniRoutes.homeRoute);
              },
            ),
            DrawerListTile(
              title: "Notification",
              icon: Icon(Icons.notifications, color: Color(0xFF1C4E80)),
              press: () {
                Navigator.of(context)
                    .pushNamed(AstegniRoutes.notificationsRoute);
              },
            ),
            DrawerListTile(
              title: "Tutors",
              icon: Icon(Icons.list, color: Color(0xFF930077)),
              press: () {
                Navigator.of(context).pushNamed(AstegniRoutes.tutorsRoute);
              },
            ),
            DrawerListTile(
              title: "Students",
              icon: Icon(Icons.list, color: Color(0xFFD32D41)),
              press: () {
                Navigator.of(context).pushNamed(AstegniRoutes.studentsRoute);
              },
            ),
          ],
        ),
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  const DrawerListTile({
    Key? key,
    required this.title,
    required this.icon,
    required this.press,
  }) : super(key: key);

  final String title;
  final Widget icon;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: press,
      horizontalTitleGap: 0.0,
      leading: icon,
      title: Text(
        title,
        // style: TextStyle(color: Colors.white54),
      ),
    );
  }
}
