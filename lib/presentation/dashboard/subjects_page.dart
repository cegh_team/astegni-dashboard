// import 'package:astegni_dashboard/data/models/tutor.dart';
// import 'package:astegni_dashboard/presentation/dashboard/constants.dart';
// import 'package:astegni_dashboard/presentation/dashboard/side_menu.dart';
// import 'package:data_table_2/data_table_2.dart';
// import 'package:flutter/material.dart';

// import '../responsive.dart';
// import 'header.dart';

// class SubjectsPage extends StatelessWidget {
//   const SubjectsPage({
//     Key? key,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       drawer: SideMenu(),
//       body: SafeArea(
//         child: Row(
//           children: [
//             if (Responsive.isDesktop(context))
//               Expanded(
//                 child: SideMenu(),
//               ),
//             Expanded(
//               flex: 5,
//               child: _subjectsPageBody(context),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }

// Widget _subjectsPageBody(context) {
//   return SafeArea(
//     child: Container(
//       // color: Theme.of(context).backgroundColor,
//       padding: EdgeInsets.all(defaultPadding),
//       decoration: BoxDecoration(
//         color: Theme.of(context).backgroundColor,
//         borderRadius: const BorderRadius.all(Radius.circular(10)),
//       ),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           Header(),
//           SizedBox(height: 16),
//           Row(
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: [
//               Text(
//                 "Subjects",
//                 style: Theme.of(context).textTheme.subtitle1,
//               ),
//               ElevatedButton.icon(
//                 style: TextButton.styleFrom(
//                   padding: EdgeInsets.symmetric(
//                     horizontal: defaultPadding * 1.5,
//                     vertical:
//                         defaultPadding / (Responsive.isMobile(context) ? 2 : 1),
//                   ),
//                 ),
//                 onPressed: () {},
//                 icon: Icon(Icons.add),
//                 label: Text("Add New"),
//               ),
//             ],
//           ),
//           SizedBox(
//             width: double.infinity,
//             child: DataTable2(
//               columnSpacing: defaultPadding,
//               minWidth: 600,
//               columns: [
//                 DataColumn(
//                   label: Text("Name"),
//                 ),
//                 DataColumn(
//                   label: Text("Address"),
//                 ),
//                 DataColumn(
//                   label: Text("Status"),
//                 ),
//                 DataColumn(
//                   label: Text("Actions"),
//                 ),
//               ],
//               rows: List.generate(
//                 demoTutors.length,
//                 (index) => subjectDataRow(demoTutors[index], context),
//               ),
//             ),
//           ),
//         ],
//       ),
//     ),
//   );
// }

// DataRow subjectDataRow(tutors tutor, context) {
//   return DataRow(
//     cells: [
//       DataCell(
//         Row(
//           children: [
//             Container(height: 20, child: Image.asset(tutor.profilePicture!)),
//             Padding(
//               padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
//               child: Text(tutor.name!),
//             ),
//           ],
//         ),
//       ),
//       DataCell(Text(tutor.address!)),
//       //show using on/off button
//       DataCell((tutor.status! == 'active')
//           ? Icon(Icons.hdr_on_select_rounded)
//           : Icon(Icons.hdr_off_select_rounded)),
//       DataCell(
//         Row(
//           children: [
//             Icon(
//               Icons.details,
//               color: Theme.of(context).primaryColor,
//             ),
//             SizedBox(
//               width: 10,
//             ),
//             Icon(
//               Icons.edit,
//               color: Theme.of(context).primaryColor,
//             ),
//             SizedBox(
//               width: 10,
//             ),
//             Icon(
//               Icons.delete,
//               color: Colors.red,
//             ),
//           ],
//         ),
//       ),
//     ],
//   );
// }
