import 'package:astegni_dashboard/data/repositaries/repository.dart';
import 'package:astegni_dashboard/logic/blocs/authentication/authentication_bloc.dart';
import 'package:astegni_dashboard/logic/blocs/login/login_bloc.dart';
import 'package:astegni_dashboard/presentation/login/login_fom.dart';
import 'package:astegni_dashboard/presentation/router/route_constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Color(0xFFf7f7f7),
      body: BlocProvider(
        create: (context) => LoginBloc(
            authRepository: context.read<AuthRepository>(),
            authenticationBloc: context.read<AuthenticationBloc>()),
        child: BlocListener<LoginBloc, LoginState>(
          listener: (context, state) {
            if (state.status.isSubmissionSuccess) {
              Navigator.of(context).popAndPushNamed(AstegniRoutes.homeRoute);
            }
          },
          child: Stack(
            children: [
              Container(
                child: SizedBox(
                    height: screenSize.height * 0.2,
                    width: screenSize.width,
                    child: Container()
                    // Image.asset(
                    //   'assets/images/background.jpg',
                    //   fit: BoxFit.cover,
                    // ),
                    ),
              ),
              Center(
                child: Container(
                    width: screenSize.width >= 1000
                        ? screenSize.width * 0.4
                        : screenSize.width * 0.8,
                    child: LoginForm()),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
