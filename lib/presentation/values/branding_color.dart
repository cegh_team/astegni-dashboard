import 'package:flutter/material.dart';

MaterialColor brandingColor = MaterialColor(
  brandingPrimaryColor,
  <int, Color>{
    50: Color.fromRGBO(66, 103, 178, 0.1),
    100: Color.fromRGBO(66, 103, 178, 0.2),
    200: Color.fromRGBO(66, 103, 178, 0.3),
    300: Color.fromRGBO(66, 103, 178, 0.4),
    400: Color.fromRGBO(66, 103, 178, 0.5),
    500: Color(brandingPrimaryColor),
    600: Color.fromRGBO(66, 103, 178, 0.7),
    700: Color.fromRGBO(66, 103, 178, 0.8),
    800: Color.fromRGBO(66, 103, 178, 0.9),
    900: Color.fromRGBO(66, 103, 178, 1.0),
  },
);
int brandingPrimaryColor = 0xFF4267B2; 

// #f7f7f7
//62, 180, 137    0xFF3EB489

//green light (62,180,137)  #3EB489



