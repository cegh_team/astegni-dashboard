class AstegniRoutes {
  static const String loginRoute = "logIn";
  static const String homeRoute = "home";
  static const String tutorsRoute = "tutors";
  static const String studentsRoute = "students";
  static const String notificationsRoute = "notifications";
  static const String subjectsRoute = "subjects";
  static const String addUpdateRoute = "addUpdate";
}
