import 'package:astegni_dashboard/presentation/dashboard/add_update_tutor_page.dart';

import '../login/login_page.dart';
import '../notfound/not_found_page.dart';

import 'route_constants.dart';
import 'package:flutter/material.dart';

class AppRouter {
  Route? onGenerateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      // case AstegniRoutes.homeRoute:
      //   return MaterialPageRoute(builder: (_) => Dashboard());
      case AstegniRoutes.loginRoute:
        return MaterialPageRoute(builder: (_) => LoginPage());
      // case AstegniRoutes.tutorsRoute:
      //   return MaterialPageRoute(builder: (_) => TutorsPage());
      // case AstegniRoutes.studentsRoute:
      //   return MaterialPageRoute(builder: (_) => StudentsPage());
      // case AstegniRoutes.notificationsRoute:
      //   return MaterialPageRoute(builder: (_) => NotificationsPage());
      case AstegniRoutes.addUpdateRoute:
        return MaterialPageRoute(builder: (_) => AddUpdateTutorPage());
      default:
        return MaterialPageRoute(builder: (_) => NotFoundPage());
    }
  }
}
