import 'package:astegni_dashboard/data/models/notification.dart';
import 'package:equatable/equatable.dart';

class NotificationState extends Equatable {
  //const NotificationState();
  final List<NotificationModel> notifications = [];

  @override
  List<Object> get props => [];
}

class NotificationLoading extends NotificationState {}

class NotificationsLoadSuccess extends NotificationState {
  final List<NotificationModel> notifications;

  NotificationsLoadSuccess([this.notifications = const []]);

  @override
  List<Object> get props => [notifications];
}

class NotificationNotLoaded extends NotificationState {}

class NotificationOperationFailure extends NotificationState {}
