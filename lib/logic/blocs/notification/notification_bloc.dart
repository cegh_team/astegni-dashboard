import 'package:astegni_dashboard/data/repositaries/notification_repository.dart';
import 'package:astegni_dashboard/logic/blocs/notification/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NotificationBloc extends Bloc<NotificationEvent, NotificationState> {
  final NotificationRepository notificationRepository;

  NotificationBloc({required this.notificationRepository})
      : super(NotificationNotLoaded());
  @override
  void onTransition(
      Transition<NotificationEvent, NotificationState> transition) {
    print(transition);
    super.onTransition(transition);
  }

  @override
  Stream<NotificationState> mapEventToState(NotificationEvent event) async* {
    if (event is NotificationLoad) {
      if (state is NotificationNotLoaded) {
        yield NotificationLoading();
        try {
          final notifications =
              await notificationRepository.loadNotifications();

          yield NotificationsLoadSuccess(notifications);
        } catch (e) {
          print(e);
          yield NotificationOperationFailure();
        }
      }
    }
  }
}
