import 'package:astegni_dashboard/data/models/user.dart';
import 'package:equatable/equatable.dart';

abstract class TutorEvent extends Equatable {
  const TutorEvent();
}

class TutorLoad extends TutorEvent {
  const TutorLoad();

  @override
  List<Object> get props => [];
}

class TutorSubmitted extends TutorEvent {
  const TutorSubmitted();
  @override
  List<Object> get props => [];
}

class TutorCreate extends TutorEvent {
  final User tutor;

  const TutorCreate(this.tutor);

  @override
  List<Object> get props => [tutor];

  @override
  String toString() => 'tutor Created {tutor: $tutor}';
}

class TutorUpdate extends TutorEvent {
  final User tutor;

  const TutorUpdate(this.tutor);

  @override
  List<Object> get props => [tutor];

  @override
  String toString() => 'Tutor Updated {Tutor: $tutor}';
}

class TutorStatusUpdate extends TutorEvent {
  final String userId;
  final String status;

  const TutorStatusUpdate(this.userId, this.status);

  @override
  List<Object> get props => [userId, status];

  @override
  String toString() => 'Tutor Updated {Tutor: $status}';
}

class TutorDelete extends TutorEvent {
  final User tutor;

  const TutorDelete(this.tutor);

  @override
  List<Object> get props => [tutor];

  @override
  toString() => 'Tutor Deleted {Tutor: $tutor}';
}

class TutorFirstNameChanged extends TutorEvent {
  const TutorFirstNameChanged(this.firstName);

  final String firstName;

  @override
  List<Object> get props => [firstName];
}

class TutorLastNameChanged extends TutorEvent {
  const TutorLastNameChanged(this.lastName);

  final String lastName;

  @override
  List<Object> get props => [lastName];
}

class TutorEmailChanged extends TutorEvent {
  const TutorEmailChanged(this.email);

  final String email;

  @override
  List<Object> get props => [email];
}

class TutorPasswordChanged extends TutorEvent {
  const TutorPasswordChanged(this.password);

  final String password;

  @override
  List<Object> get props => [password];
}

class TutorRoleChanged extends TutorEvent {
  const TutorRoleChanged(this.role);

  final String role;

  @override
  List<Object> get props => [role];
}

class TutorGenderChanged extends TutorEvent {
  const TutorGenderChanged(this.gender);

  final String gender;

  @override
  List<Object> get props => [gender];
}

class TutorUserBioChanged extends TutorEvent {
  const TutorUserBioChanged(this.userBio);

  final String userBio;

  @override
  List<Object> get props => [userBio];
}

class TutorPricingChanged extends TutorEvent {
  const TutorPricingChanged(this.pricing);

  final String pricing;

  @override
  List<Object> get props => [pricing];
}

class TutorAvailabilityChanged extends TutorEvent {
  const TutorAvailabilityChanged({required this.availability});

  final String availability;

  @override
  List<Object> get props => [availability];
}

class TutorAddressChanged extends TutorEvent {
  const TutorAddressChanged(this.address);

  final String address;

  @override
  List<Object> get props => [address];
}
