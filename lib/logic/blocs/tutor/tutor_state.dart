import 'package:astegni_dashboard/data/models/user.dart';
import 'package:equatable/equatable.dart';

class TutorState extends Equatable {
  final List<User> tutors = [];

  TutorState(
      {this.firstname = '',
      this.lastname = '',
      this.email = '',
      this.role = '',
      this.password = '',
      this.gender = '',
      this.userBio = '',
      this.pricing = '',
      this.availability = '',
      this.address = '',
      this.phone = ''});

  final String firstname;
  final String lastname;
  final String email;
  final String password;
  final String role;
  final String gender;
  final String userBio;
  final String pricing;
  final String availability;
  final String address;
  final String phone;

  TutorState copyWith({
    String? firstName,
    String? lastName,
    String? email,
    String? password,
    String? role,
    String? gender,
    String? userBio,
    String? pricing,
    String? availability,
    String? address,
    String? phone,
  }) {
    return TutorState(
      firstname: gender ?? this.gender,
      lastname: gender ?? this.gender,
      email: gender ?? this.gender,
      password: gender ?? this.gender,
      role: gender ?? this.gender,
      gender: gender ?? this.gender,
      userBio: userBio ?? this.userBio,
      pricing: pricing ?? this.pricing,
      availability: availability ?? this.availability,
      address: address ?? this.address,
      phone: phone ?? this.phone,
    );
  }

  @override
  List<Object> get props => [];
}

class TutorLoading extends TutorState {}

class TutorsLoadSuccess extends TutorState {
  final List<User> tutors;

  TutorsLoadSuccess([this.tutors = const []]);

  @override
  List<Object> get props => [tutors];
}

class TutorNotLoaded extends TutorState {}

class TutorOperationFailure extends TutorState {}
