import 'package:astegni_dashboard/data/models/user.dart';
import 'package:astegni_dashboard/data/repositaries/tutor_repository.dart';
import 'package:astegni_dashboard/logic/blocs/tutor/tutor_event.dart';
import 'package:astegni_dashboard/logic/blocs/tutor/tutor_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TutorBloc extends Bloc<TutorEvent, TutorState> {
  final TutorRepository tutorRepository;

  TutorBloc({required this.tutorRepository}) : super(TutorNotLoaded());
  @override
  void onTransition(Transition<TutorEvent, TutorState> transition) {
    print(transition);
    super.onTransition(transition);
  }

  @override
  Stream<TutorState> mapEventToState(TutorEvent event) async* {
    if (event is TutorFirstNameChanged) {
      yield state.copyWith(firstName: event.firstName);
    } else if (event is TutorLastNameChanged) {
      yield state.copyWith(lastName: event.lastName);
    } else if (event is TutorEmailChanged) {
      yield state.copyWith(email: event.email);
    } else if (event is TutorPasswordChanged) {
      yield state.copyWith(password: event.password);
    } else if (event is TutorRoleChanged) {
      yield state.copyWith(role: event.role);
    } else if (event is TutorUserBioChanged) {
      yield state.copyWith(userBio: event.userBio);
    } else if (event is TutorGenderChanged) {
      yield state.copyWith(gender: event.gender);
    } else if (event is TutorPricingChanged) {
      yield state.copyWith(pricing: event.pricing);
    } else if (event is TutorAddressChanged) {
      yield state.copyWith(address: event.address);
    } else if (event is TutorAvailabilityChanged) {
      yield state.copyWith(availability: event.availability);
    } else if (event is TutorLoad) {
      if (state is TutorNotLoaded) {
        yield TutorLoading();
        try {
          final List<User> tutors = await tutorRepository.loadTutors();
          yield TutorsLoadSuccess(tutors);
        } catch (e) {
          print(e);
          yield TutorOperationFailure();
        }
      }
    }

    if (event is TutorDelete) {
      try {
        await tutorRepository.deleteTutor(event.tutor.userId);

        // add(TutorLoad());
        final List<User> tutors = await tutorRepository.loadTutors();
        yield TutorsLoadSuccess(tutors);
      } catch (_) {
        yield TutorOperationFailure();
      }
    }

    if (event is TutorStatusUpdate) {
      try {
        await tutorRepository.updateTutorStatus(event.status, event.userId);

        // add(TutorLoad());
        final List<User> tutors = await tutorRepository.loadTutors();
        yield TutorsLoadSuccess(tutors);
      } catch (_) {
        yield TutorOperationFailure();
      }
    }
  }
}
