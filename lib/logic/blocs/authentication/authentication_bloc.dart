import 'package:astegni_dashboard/data/models/user.dart';
import 'package:astegni_dashboard/data/repositaries/repository.dart';
import 'package:astegni_dashboard/utilities/local_storage.dart';
import 'package:astegni_dashboard/utilities/storage_keys.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
part 'authentication_event.dart';

part 'authentication_state.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final AuthRepository authRepository;
  @override
  AuthenticationBloc({required this.authRepository}) : super(Uninitialized());

  @override
  void onTransition(
      Transition<AuthenticationEvent, AuthenticationState> transition) {
    print(transition);
    super.onTransition(transition);
  }

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    // app start
    if (event is AppStartedFromCache) {
      yield Authenticated(user: event.user);
    } else if (event is AppStarted) {
      if (state is Uninitialized) {
        yield Unauthenticated();
      }
    } else if (event is UserUpdated) {
      var token = LocalStorage.getItem(TOKEN);

      try {
        print("Calling getUser to update............................");
        User currentUsr = await authRepository.getUser(token: token!);

        yield Authenticated(user: currentUsr);
      } catch (e) {
        print(e.toString());
      }
    } else if (event is LoggedIn) {
      yield Authenticated(user: event.user);
      LocalStorage.setItem(TOKEN, event.token);
      LocalStorage.setItem(USERID, event.user.userId);

      //initialize
      // FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
      // _firebaseMessaging.getToken().then((value) {
      //   //update if device token is different from the current one
      //   if (value! != event.user.deviceToken) {
      //     print('()()()${event.user.userId} token:${value.length}');
      //     ProfileRepository().createOrUpdateUserProfile(
      //       userId: event.user.userId,
      //       gender: event.user.gender,
      //       userBio: event.user.bio,
      //       pricing: event.user.pricing,
      //       address: event.user.address,
      //       availability: event.user.availability,
      //       deviceToken: value,
      //       subjects: Utility().subjectsToStringList(event.user.subjects, []),
      //     );
      //   }

      // print("Device token: $value");
      // });

      // FirebaseMessaging.onMessage.listen((RemoteMessage event) {
      //   print("message recieved");
      //   print(event.notification!.body);
      // });
      // FirebaseMessaging.onMessageOpenedApp.listen((message) {
      //   print('Message clicked!');
      // });

      //yield Authenticated(user: event.user);
    } else if (event is LoggedOut) {
      yield Unauthenticated();
      LocalStorage.removeItem(TOKEN);
      LocalStorage.removeItem(USERID);
    }
  }

  @override
  AuthenticationState? fromJson(Map<String, dynamic> json) {
    try {
      // print(json);
      add(AppStartedFromCache(user: User.fromJson(json)));

      return null;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  @override
  Map<String, dynamic>? toJson(AuthenticationState state) {
    if (state is Authenticated) {
      print('TOOOOOOOOOOOOOOOOOOOOJSOOOOOOOON');
      return state.user.toJson();
    } else {
      return null;
    }
  }
}
