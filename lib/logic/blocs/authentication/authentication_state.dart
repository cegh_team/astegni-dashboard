part of 'authentication_bloc.dart';

abstract class AuthenticationState extends Equatable {
  @override
  List<Object> get props => [];
}

class Uninitialized extends AuthenticationState {}

class Authenticated extends AuthenticationState {
  Authenticated({required this.user});

  final User user;

  @override
  List<Object> get props => [user];
}

class Unauthenticated extends AuthenticationState {}
