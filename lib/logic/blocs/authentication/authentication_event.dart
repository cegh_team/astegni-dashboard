part of 'authentication_bloc.dart';

abstract class AuthenticationEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class AppStarted extends AuthenticationEvent {
  // final User user;
  // AppStarted({required this.user});
}

class AppStartedFromCache extends AuthenticationEvent {
  final User user;
  AppStartedFromCache({required this.user});
}

class UserUpdated extends AuthenticationEvent {}

class LoggedIn extends AuthenticationEvent {
  final String token;

  final User user;

  LoggedIn({
    required this.token,
    required this.user,
  });

  //@override
  //List<Object> get props => [token, role, user, uid];
  @override
  List<Object> get props => [
        token,
        user,
      ];
}

class LoggedOut extends AuthenticationEvent {}
