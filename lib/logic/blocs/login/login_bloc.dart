import 'dart:convert';
import 'package:astegni_dashboard/data/models/subject.dart';
import 'package:astegni_dashboard/data/models/user.dart';
import 'package:astegni_dashboard/data/models/validation_models.dart';
import 'package:astegni_dashboard/data/repositaries/repository.dart';
import 'package:astegni_dashboard/logic/blocs/authentication/authentication.dart';
import 'package:equatable/equatable.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  /*@override
  void onTransition(Transition<LoginEvent, LoginState> transition) {
    print(transition);
    super.onTransition(transition);
  }*/

  final AuthRepository authRepository;
  final AuthenticationBloc authenticationBloc;

  LoginBloc({required this.authenticationBloc, required this.authRepository})
      : super(const LoginState());

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginEmailChanged) {
      final email = Email.dirty(event.email);
      yield state.copyWith(
        email: email.valid ? email : Email.pure(event.email),
        status: Formz.validate([email, state.password]),
      );
    } else if (event is LoginPasswordChanged) {
      final password = Password.dirty(event.password);
      yield state.copyWith(
        password: password.valid ? password : Password.pure(event.password),
        status: Formz.validate([state.email, password]),
      );
    } else if (event is EmailUnfocused) {
      final email = Email.dirty(state.email.value);
      yield state.copyWith(
        email: email,
        status: Formz.validate([email, state.password]),
      );
    } else if (event is PasswordUnfocused) {
      final password = Password.dirty(state.password.value);
      yield state.copyWith(
        password: password,
        status: Formz.validate([state.email, password]),
      );
    } else if (event is LoginFormSubmitted) {
      yield* _mapLoginSubmittedToState(event, state);
    }
  }

  Stream<LoginState> _mapLoginSubmittedToState(
    LoginFormSubmitted event,
    LoginState state,
  ) async* {
    final email = Email.dirty(state.email.value);
    final password = Password.dirty(state.password.value);
    yield state.copyWith(
      email: email,
      password: password,
      status: Formz.validate([email, password]),
    );

    if (state.status.isValidated) {
      yield state.copyWith(status: FormzStatus.submissionInProgress);
      try {
        final response = await authRepository.login(
          //await authenticationBloc.authRepository.login(
          email: state.email.value,
          password: state.password.value,
        );

        var _response = jsonDecode(response.body);
        //print(_response);

        print('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
        print(_response['user']['subjects']['data']
            .map((subject) => Subject.fromJson(subject)));
        print('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
        //print(_response['user']);
        User usr = User.fromJson(_response['user']);

        authenticationBloc.add(LoggedIn(token: _response['token'], user: usr));

        yield state.copyWith(status: FormzStatus.submissionSuccess);
      } catch (e) {
        print(e.toString());
        yield state.copyWith(status: FormzStatus.submissionFailure);
      }

      /*on SocketException {
        yield state.copyWith(error: "No internet connection");
        yield state.copyWith(status: FormzStatus.submissionFailure);
      } on CustomError catch (e) {
        yield state.copyWith(status: FormzStatus.submissionFailure);
        if (e.status == 422) {
          yield state.copyWith(
              error: jsonDecode(e.message)['message'].toString());
        } else if (e.status == 401) {
          yield state.copyWith(
              error: jsonDecode(e.message)['message'].toString());
        } else {
          yield state.copyWith(error: "Sorry, unkown error is occured");
        }
      } on Exception {
        yield state.copyWith(status: FormzStatus.submissionFailure);
        yield state.copyWith(error: "Couldn't reach the server");
      }*/
    }
  }
}
