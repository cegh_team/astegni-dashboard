import 'package:astegni_dashboard/data/dataproviders/auth_api_provider.dart';
import 'package:astegni_dashboard/data/models/user.dart';

class AuthRepository {
  AuthApiProvider _authApiProvider = AuthApiProvider();

  Future<dynamic> login(
      {required String email, required String password}) async {
    print('trying to log in');
    // await Future.delayed(Duration(milliseconds: 200));
    final response = _authApiProvider.login(email, password);

    print('logged in');

    return response;
  }

  Future<User> getUser({required String token}) async {
    //await Future.delayed(Duration(milliseconds: 200));
    print('Trying to call GetUser');
    final response = _authApiProvider.getUser(token);
    print('Method Called');

    return response;
  }
}
