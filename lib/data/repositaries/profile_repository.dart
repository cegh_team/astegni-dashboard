import 'dart:io';

import 'package:astegni_dashboard/data/dataproviders/profile_api_provider.dart';
import 'package:astegni_dashboard/utilities/base_url.dart';
import 'package:astegni_dashboard/utilities/local_storage.dart';
import 'package:astegni_dashboard/utilities/storage_keys.dart';
import 'package:http/http.dart' as http;

class ProfileRepository {
  ProfileApiProvider _profileApiProvider = ProfileApiProvider();

  var token = LocalStorage.getItem(TOKEN);

  Future<dynamic> createOrUpdateUserProfile({
    required String userId,
    required String gender,
    required String userBio,
    required String pricing,
    required String address,
    required String availability,
    required String deviceToken,
    required List<String> subjects,
  }) async {
    final response = _profileApiProvider.createOrUpdateUserProfile(
      userId,
      gender,
      address,
      pricing,
      userBio,
      availability,
      deviceToken,
      subjects,
    );

    return response;
  }

  Future<String?> uploadImage(filepath, userId) async {
    Map<String, String> headers = {
      HttpHeaders.authorizationHeader: 'Bearer $token'
    };

    var request = http.MultipartRequest(
        'POST', Uri.parse(BASE_URL + "/api/profile/$userId"));
    request.headers.addAll(headers);
    request.files.add(http.MultipartFile('image',
        File(filepath).readAsBytes().asStream(), File(filepath).lengthSync(),
        filename: filepath.split("/").last));

    http.Response response =
        await http.Response.fromStream(await request.send());

    return response.body;
  }
}
