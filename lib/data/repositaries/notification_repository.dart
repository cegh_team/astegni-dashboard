import 'package:astegni_dashboard/data/dataproviders/notification_api_provider.dart';

class NotificationRepository {
  NotificationApiProvider _notificationApiProvider = NotificationApiProvider();

  Future<dynamic> loadNotifications() async {
    final response = _notificationApiProvider.loadNotifications();

    return response;
  }
}
