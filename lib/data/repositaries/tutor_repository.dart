import 'package:astegni_dashboard/data/dataproviders/tutor_api_provider.dart';

class TutorRepository {
  TutorApiProvider _tutorApiProvider = TutorApiProvider();

  Future<dynamic> loadTutors() async {
    final response = _tutorApiProvider.loadTutors();

    return response;
  }

  Future<dynamic> deleteTutor(String id) async {
    final response = _tutorApiProvider.deleteTutor(id);

    return response;
  }

  Future<dynamic> updateTutorStatus(String status, String id) async {
    final response = _tutorApiProvider.updateTutorStatus(id, status);

    return response;
  }
}
