class Tutor {
  final String? profilePicture, name, address, status;

  Tutor({this.profilePicture, this.name, this.address, this.status});

  factory Tutor.fromJson(Map<String, dynamic> json) {
    return Tutor(
      name: json['first_name'],
      profilePicture: json['profile_picture'],
      address: json['address'] == null ? "" : json['address'],
      status: json['bio'] == null ? "" : json['bio'],
    );
  }
}
