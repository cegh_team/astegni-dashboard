class NotificationModel {
  final String senderId;
  final String recieverId;
  final String title;
  final String content;
  final String image;

  const NotificationModel({
    this.senderId = '',
    this.recieverId = '',
    this.title = '',
    this.content = '',
    this.image = '',
  });

  factory NotificationModel.fromJson(Map<String, dynamic> json) {
    return NotificationModel(
      senderId: json['sender_id'],
      recieverId: json['reciever_id'],
      title: json['title'],
      content: json['body'],
      image: json['image'],
    );
  }
}
