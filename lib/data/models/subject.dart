class Subject {
  final String name;
  final String type;

  Subject({this.name = '', this.type = ''});

  factory Subject.fromJson(Map<String, dynamic> json) {
    return Subject(
      name: json['name'] == null ? "" : json['name'],
      type: json['type'] == null ? "" : json['type'],
    );
  }
  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'type': type,
    };
  }

  @override
  String toString() {
    return "name: $name , type: $type";
  }
}
