enum Role { STUDENT, TUTOR, ADMIN }
enum Gender { MALE, FEMALE }

extension ParseToString on Role {
  String toShortString() {
    return this.toString().split('.').last.toLowerCase();
  }
}

extension ParseToStringGender on Gender {
  String toShortString() {
    return this.toString().split('.').last.toLowerCase();
  }
}
