import 'package:astegni_dashboard/data/models/subject.dart';

class User {
  final String userId;
  final String firstName;
  final String lastName;
  final String email;
  final String profilePicture;
  final String address;
  final String bio;
  final String pricing;
  final String availability;
  final String gender;
  final String status;
  final String deviceToken;
  final String phone;
  final List<Subject> subjects;

  User({
    required this.userId,
    this.firstName = '',
    this.lastName = '',
    required this.email,
    this.profilePicture = '',
    this.address = '',
    this.bio = '',
    this.pricing = '',
    this.availability = '',
    this.gender = '',
    this.status = '',
    this.deviceToken = '',
    this.phone = '',
    this.subjects = const <Subject>[],
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      userId: json['user_id'],
      firstName: json['first_name'],
      lastName: json['last_name'],
      email: json['email'],
      profilePicture: json['profile_picture'],
      address: json['address'] == null ? "" : json['address'],
      bio: json['bio'] == null ? "" : json['bio'],
      pricing: json['pricing'] == null ? "" : json['pricing'],
      availability: json['availability'] == null ? "" : json['availability'],
      gender: json['gender'] == null ? "" : json['gender'],
      status: json['status'] == null ? "" : json['status'],
      phone: json['phone'] == null ? "" : json['phone'],
      subjects: json['subjects']['data'].isEmpty
          ? <Subject>[]
          : List<Subject>.from(
              json['subjects']['data'].map((item) => Subject.fromJson((item)))),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'user_id': userId,
      'first_name': firstName,
      'last_name': lastName,
      'email': email,
      'profile_picture': profilePicture,
      'address': address,
      'bio': bio,
      'pricing': pricing,
      'availability': availability,
      'gender': gender,
      'subjects': {'data': this.subjects.map((i) => i.toJson()).toList()},
    };
  }
}
