import 'dart:io';

class CustomError implements HttpException {
  final message;
  final int status;

  CustomError(this.status, this.message);

  @override
  String toString() => 'CustomError(status: $status, message: $message)';

  @override
  Uri? get uri => throw UnimplementedError();
}

dynamic networkError(CustomError e) {
  // yield state.copyWith(status: FormzStatus.submissionFailure);
  if (e.status == 422) {
    print(e.message);
  } else if (e.status == 401) {
    print(e.message);
  } else {
    print("Unknown error occured");
  }
}
