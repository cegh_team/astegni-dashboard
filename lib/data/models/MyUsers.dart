class DataInfo {
  final String? title;
  final int? numOfUsers;

  DataInfo({this.title, this.numOfUsers});
}

List demoMyUsers = [
  DataInfo(
    title: "Total Users",
    numOfUsers: 20,
  ),
  DataInfo(
    title: "Active Users",
    numOfUsers: 3,
  ),
  DataInfo(
    title: "Tutors",
    numOfUsers: 5,
  ),
  DataInfo(
    title: "Students",
    numOfUsers: 15,
  ),
];
