import 'package:astegni_dashboard/presentation/values/images.dart';

class TutorsData {
  final String? profilePicture, name, address, status;

  TutorsData({this.profilePicture, this.name, this.address, this.status});
}

List demoTutors = [
  TutorsData(
    profilePicture: Images.profile,
    name: "Charles Darwin",
    address: "Addis Ababa",
    status: "Active",
  ),
  TutorsData(
    profilePicture: Images.profile,
    name: "Charles Darwin",
    address: "Addis Ababa",
    status: "Active",
  ),
  TutorsData(
    profilePicture: Images.profile,
    name: "Charles Darwin",
    address: "Addis Ababa",
    status: "Active",
  ),
  TutorsData(
    profilePicture: Images.profile,
    name: "Charles Darwin",
    address: "Addis Ababa",
    status: "Active",
  ),
  TutorsData(
    profilePicture: Images.profile,
    name: "Charles Darwin",
    address: "Addis Ababa",
    status: "Active",
  ),
  TutorsData(
    profilePicture: Images.profile,
    name: "Charles Darwin",
    address: "Addis Ababa",
    status: "Active",
  ),
  TutorsData(
    profilePicture: Images.profile,
    name: "Charles Darwin",
    address: "Addis Ababa",
    status: "Active",
  ),
];
