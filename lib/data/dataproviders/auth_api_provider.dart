import 'dart:convert';
import 'dart:io';

import 'package:astegni_dashboard/data/models/errors/custom_error.dart';
import 'package:astegni_dashboard/data/models/user.dart';
import 'package:astegni_dashboard/utilities/base_url.dart';
import 'package:http/http.dart' as http;

class AuthApiProvider {
  Future<dynamic> login(String email, String password) async {
    var url = Uri.parse(BASE_URL + "/api/auth/login");

    final response =
        await http.post(url, body: {'email': email, 'password': password});

    if (response.statusCode != 200)
      throw CustomError(response.statusCode, response.body);

    return response;
  }

  Future<User> getUser(String token) async {
    var url = Uri.parse(BASE_URL + "/api/auth/user");

    final response = await http
        .get(url, headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});

    if (response.statusCode != 200) {
      throw CustomError(response.statusCode, response.body);
    }
    final responseJson = jsonDecode(response.body);
    //return User.fromJson(responseJson['user']);
    return User.fromJson(responseJson['data']);
  }
}
