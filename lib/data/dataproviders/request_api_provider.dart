// import 'dart:convert';
// import 'dart:io';

// import 'package:astegni_dashboard/data/models/errors/custom_error.dart';
// import 'package:astegni_dashboard/data/models/request.dart';
// import 'package:astegni_dashboard/utilities/base_url.dart';
// import 'package:astegni_dashboard/utilities/local_storage.dart';
// import 'package:astegni_dashboard/utilities/storage_keys.dart';
// import 'package:http/http.dart' as http;

// class RequestApiProvider {
//   Future<List<Request>> loadRecievedRequests() async {
//     var token = LocalStorage.getItem(TOKEN);
//     var tutorId = LocalStorage.getItem(USERID);
//     var url = Uri.parse(BASE_URL + "/api/requests/$tutorId/recieved");
//     final response = await http.get(url, headers: {
//       HttpHeaders.authorizationHeader: 'Bearer $token',
//     });

//     print(response.body);

//     if (response.statusCode == 200) {
//       return (jsonDecode(response.body)['data'] as List)
//           .map((request) => Request.fromJson(request))
//           .toList();
//     } else {
//       throw CustomError(response.statusCode, response.body);
//     }
//   }

//   Future<dynamic> createOrUpdateRequest(
//     String tutorId,
//     String content,
//   ) async {
//     var url = Uri.parse(BASE_URL + "/api/requests");
//     var token = LocalStorage.getItem(TOKEN);
//     var studentId = LocalStorage.getItem(USERID);

//     print("............................................before sending request");
//     final response = await http.post(url, headers: {
//       HttpHeaders.authorizationHeader: 'Bearer $token',
//     }, body: {
//       'student_id': studentId,
//       'tutor_id': tutorId,
//       'content': content
//     });

//     // print(response.body);

//     if (response.statusCode != 200)
//       throw CustomError(response.statusCode, response.body);

//     return response;
//   }
// }
