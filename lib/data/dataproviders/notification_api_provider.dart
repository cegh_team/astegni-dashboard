import 'dart:convert';
import 'dart:io';
import 'package:astegni_dashboard/data/models/errors/custom_error.dart';
import 'package:astegni_dashboard/data/models/notification.dart';
import 'package:astegni_dashboard/utilities/base_url.dart';
import 'package:astegni_dashboard/utilities/local_storage.dart';
import 'package:astegni_dashboard/utilities/storage_keys.dart';
import 'package:http/http.dart' as http;

class NotificationApiProvider {
  Future<List<NotificationModel>> loadNotifications() async {
    var token = LocalStorage.getItem(TOKEN);
    var recieverId = LocalStorage.getItem(USERID);
    var url = Uri.parse(BASE_URL + "/api/notifications/$recieverId");

    final response = await http.get(url, headers: {
      HttpHeaders.authorizationHeader: 'Bearer $token',
    });

    print(response.body);

    if (response.statusCode == 200) {
      return (jsonDecode(response.body)['data'] as List)
          .map((notification) => NotificationModel.fromJson(notification))
          .toList();
    } else {
      throw CustomError(response.statusCode, response.body);
    }
  }
}
