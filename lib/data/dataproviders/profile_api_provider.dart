import 'dart:convert';
import 'dart:io';

import 'package:astegni_dashboard/data/models/errors/custom_error.dart';
import 'package:astegni_dashboard/utilities/base_url.dart';
import 'package:astegni_dashboard/utilities/local_storage.dart';
import 'package:astegni_dashboard/utilities/storage_keys.dart';
import 'package:http/http.dart' as http;

class ProfileApiProvider {
  Future<dynamic> createOrUpdateUserProfile(
    String userId,
    String gender,
    String address,
    String pricing,
    String userBio,
    String availability,
    String deviceToken,
    List<String> subjects,
  ) async {
    var url = Uri.parse(BASE_URL + "/api/users/$userId");
    var token = LocalStorage.getItem(TOKEN);

    print(
        "+++++++++++++++++++++++++++++++++++++++++++++${jsonEncode(subjects)}");

    final response = await http.put(url, headers: {
      HttpHeaders.authorizationHeader: 'Bearer $token',
    }, body: {
      'gender': gender,
      'address': address,
      'pricing': pricing,
      'bio': userBio,
      'availability': availability,
      'device_token': deviceToken,
      'subjects': jsonEncode(subjects),
    });

    if (response.statusCode != 200)
      throw CustomError(response.statusCode, response.body);

    return response;
  }
}
