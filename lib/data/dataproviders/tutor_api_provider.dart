import 'dart:convert';
import 'dart:io';
import 'package:astegni_dashboard/data/models/errors/custom_error.dart';
import 'package:astegni_dashboard/data/models/user.dart';
import 'package:astegni_dashboard/utilities/base_url.dart';
import 'package:astegni_dashboard/utilities/local_storage.dart';
import 'package:astegni_dashboard/utilities/storage_keys.dart';
import 'package:http/http.dart' as http;

class TutorApiProvider {
  Future<List<User>> loadTutors() async {
    var url = Uri.parse(BASE_URL + "/api/tutors");
    var token = LocalStorage.getItem(TOKEN);
    final response = await http.get(url, headers: {
      HttpHeaders.authorizationHeader: 'Bearer $token',
    });

    if (response.statusCode == 200) {
      return (jsonDecode(response.body)['data'] as List)
          .map((tutor) => User.fromJson(tutor))
          .toList();
    } else {
      throw CustomError(response.statusCode, response.body);
    }
  }

  Future<void> deleteTutor(String id) async {
    var url = Uri.parse(BASE_URL + "/api/users/$id");
    var token = LocalStorage.getItem(TOKEN);
    // print(token);
    final response = await http.delete(
      url,
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer $token',
      },
    );

    print("+++++++++++++++++ ${response.statusCode}");
    if (response.statusCode != 200) {
      throw CustomError(response.statusCode, response.body);
      // throw Exception('Failed to delete tutor.');
    }
  }

  Future<dynamic> updateTutorStatus(
    String userId,
    String status,
  ) async {
    var url = Uri.parse(BASE_URL + "/api/profile/status/$userId");
    var token = LocalStorage.getItem(TOKEN);

    final response = await http.put(url, headers: {
      HttpHeaders.authorizationHeader: 'Bearer $token',
    }, body: {
      'status': status,
    });

    print(response.body);

    if (response.statusCode != 200)
      throw CustomError(response.statusCode, response.body);

    return response;
  }
}
