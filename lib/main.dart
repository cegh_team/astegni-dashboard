import 'package:astegni_dashboard/presentation/dashboard/dashboard.dart';
import 'package:astegni_dashboard/presentation/dashboard/tutors_page.dart';
import 'package:astegni_dashboard/presentation/values/branding_color.dart';
import 'package:astegni_dashboard/utilities/local_storage.dart';
import 'package:astegni_dashboard/utilities/storage_keys.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'data/repositaries/auth_repository.dart';
import 'logic/blocs/authentication/authentication_bloc.dart';
import 'presentation/dashboard/notifications_page.dart';
import 'presentation/router/app_router.dart';
import 'presentation/router/route_constants.dart';

import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  //instantiate local storage here
  await LocalStorage.initializeSharedPreferences();

  runApp(RepositoryProvider(
    create: (context) => AuthRepository(),
    child: BlocProvider<AuthenticationBloc>(
        create: (context) =>
            AuthenticationBloc(authRepository: context.read<AuthRepository>())
              ..add(AppStarted()),
        child: MyApp()),
  ));
}

class MyApp extends StatelessWidget {
  final AppRouter _appRouter = AppRouter();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Admin Dashboard',
      theme: ThemeData(
        primarySwatch: brandingColor,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      onGenerateRoute: _appRouter.onGenerateRoute,
      initialRoute: LocalStorage.getItem(TOKEN) != null
          ? AstegniRoutes.homeRoute
          : AstegniRoutes.loginRoute,
      routes: _registerRoutes(),
    );
  }

  Map<String, WidgetBuilder> _registerRoutes() {
    return <String, WidgetBuilder>{
      AstegniRoutes.homeRoute: (context) => _buildPage(context, Dashboard()),
      AstegniRoutes.tutorsRoute: (context) => _buildPage(context, TutorsPage()),
      // AstegniRoutes.studentsRoute: (context) =>
      //     _buildPage(context, StudentsPage()),
      // AstegniRoutes.subjectsRoute: (context) =>
      //     _buildPage(context, SubjectsPage()),
      AstegniRoutes.notificationsRoute: (context) =>
          _buildPage(context, NotificationsPage()),
    };
  }
}

BlocProvider<AuthenticationBloc> _buildPage(BuildContext context, page) {
  return BlocProvider.value(
    value: BlocProvider.of<AuthenticationBloc>(context),
    child: page,
  );
}
