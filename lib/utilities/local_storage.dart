import 'package:shared_preferences/shared_preferences.dart';

class LocalStorage {
  static SharedPreferences? _sharedPreferences;

  static LocalStorage _singleton = new LocalStorage._internal();

  factory LocalStorage() {
    return _singleton;
  }

  LocalStorage._internal() {
    initializeSharedPreferences();
  }

  static Future<SharedPreferences> initializeSharedPreferences() async {
    return _sharedPreferences = await SharedPreferences.getInstance();
  }

  static Future<bool> setItem(String key, String value) async {
    return await _sharedPreferences?.setString(key, value) ?? false;
  }

  static Future<bool> setItems(String key, List<String> value) async {
    return await _sharedPreferences?.setStringList(key, value) ?? false;
  }

  static String? getItem(String key) {
    return _sharedPreferences?.getString(key) ?? null;
  }

  static List<String>? getItems(String key) {
    return _sharedPreferences?.getStringList(key) ?? null;
  }

  static void removeItem(String key) async {
    await _sharedPreferences?.remove(key);
  }
}
